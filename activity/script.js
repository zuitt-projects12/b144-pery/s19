let getCube = (num) => num ** 3;

let n = 4;
console.log(`The cube of ${n} is ${getCube(n)}`);

let address = ["221B", "Baker Street", "London", "NW1 6XE", "UK"];

let [houseNumber,street, city, zipCode, country] = address;

console.log(`I live in ${houseNumber} ${street}, ${city} ${zipCode}, ${country}.`);

let animal = {
	animalName: "Lolong",
	species: "saltwater crocodile",
	weight: 1075.00,
	length: "20 ft 3 in",
};

let {name, species, weight, length} = animal;

console.log(`${name} was a ${species}. He weighed at ${parseInt(weight)} kgs with a measurement of ${length}.`);


let coins = [0.05, 0.25, 1.00, 5.00, 10.00, 20.00];
let bills = [20.00, 50.00, 100.00, 200.00, 500.00, 1000.00];

coins.forEach(coin => console.log(`Coin: ₱ ${coin}`));
let totalCoins = coins.reduce((total, coin) => total += coin);
console.log(`Total Coins: ₱ ${totalCoins}`);

bills.forEach(bill => console.log(`Bill: ₱ ${bill} `));
let totalBills = bills.reduce((total, bill) => total += bill);
console.log(`Total Bills: ₱ ${totalBills}`);

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let dog = new Dog("Kenneth", 1, "Chihuahua");
console.log(dog);