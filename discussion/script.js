// Javascript ES6 updates

// 1. Exponent operator
let num = Math.pow(8, 2);
console.log(num);

// ES6
num = 8 ** 2;
console.log(num);

// 2. Template literals
/*
	Allows us to write strings without using the concatenation operator (+).

	It makes your code more readable than before.

	Syntax:
	`${variable}`
*/

// Pre-template literal string
let name = "RC";
let message = "Hello " + name + "! Welcome to Zuitt!";

console.log("Message without template literals: " + message);

// ES6
message = `Hello ${name}! Welcome to Zuitt!`;
console.log(`Message with template literals: ${message}`);

// Multi-line template literals
const anotherMessage = `
${name} attended a math competition
He won it by solving the problem 8 ** 2 with solution of ${num}.`;

console.log(anotherMessage);

// Template literals allow us to write strings with embedded JS expressions. ( ${} )

const interestRate = 0.1;
const principal = 1000.00;
console.log(`The interest on your savings account is ${principal * interestRate}`);

// 3. Array Destructuring
// Allows us to unpack elements in array into distinct variables. Also allows us to name array elements with variables instead of using index numbers. Helps with code readability.

/*
	Syntax
	let/const [<variableName>, <variableName>] = array;
*/

let fullName = ["Juan", "Dela", "Cruz"];

// Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

// Array Destructuring (ES6)
let [firstName, middleName, lastName, extensionName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(extensionName);

// Expressions are any valid unit of code that resolves to a value.
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);

// 4. Object Destructuring
// 
// Allows us to unpack elements in objects into distinct variables. Also allows us to name object elements with variables instead of using dot-notation. Shortens the syntax for accessing properties from objects.
/*
	Syntax:
	let/const {<propertyName>, <propertyName> ... } = object;
*/

let person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

// Pre-Object Destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`);

const {givenName, maidenName, familyName} = person

console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`);

function getFullName({ givenName, maidenName, familyName }){
	console.log(`${givenName} ${maidenName} ${familyName}`);
}

getFullName(person);

// 5. Arrow functions

// Pre-arrow / Normal functions

function printFullName(fName, mName, lName){
	console.log(fName + " " + mName + " " + lName);
}
// Function declaration
/*
	function(parameters, ...){
		statement/s;
	}
*/

printFullName("John", "D.", "Smith");

// ES6 Arrow Function
/*
	Compact alternative syntax to traditional function. Useful for code snippets where creating function will not be reused in any other portion of the code as anonymous function

	Adheres to the "DRY" principle (Don't Repeat Yourself) where there's no longer need to create a function and think of a name for function that will only be used for code snippets.
	Syntax:
	let variableName = (parameter ...) => {
		statement/s;
	};

	One liner

	let variableName = (parameter ...) => statement; (FOR ONE STATEMENT ONLY!!!)
*/

let printFullNameArrow = (fName, mName, lName) => console.log(`${fName} ${mName} ${lName}`);

printFullNameArrow ("Juan", "Dela", "Cruz");

// Arrow Functions with loops
// Pre-Arrow functions

const students = ["John", "Jane", "Joe"];

students.forEach(function(student){
	console.log(`${student} is a student.`)
});

// Arrow function
students.forEach((student) => console.log(`${student} is a student.`));

// 6. (Arrow function) IMPLICIT RETURN STATEMENT
// There are instances when you can ommit the "return statement". This works because even without the "return" statement, Javascript implicitly adds it for the result of the function
// Pre-arrow function

let add = (x, y) => {
	return x + y
};

let total = add(1, 2);
console.log(total);

// Arrow function with implicit return

add = (x, y) => x + y;

total = add(1, 2);
console.log(total);

// let filterFriends = friends.filter((friend) => friend.length === 4);

// 7. (Arrow function) Default function argument value
// It provides a default argument value if none is provided when the function is invoked.

let greet = (name = 28) => {
	return `Good morning, ${name}!`
};

console.log(greet());

// 8. Classes

/*
	Syntax:
	class className{
		constructor(objectPropertyA, objectPropertyB ... ){
			this.objectPropertyA = objectPropertyA;
			this.objectPropertyB = objectPropertyB;
		}

		functions....
	}

> The constructor is a special method of a class for creating/initializing and object for that class.
> The "this" keyword refers to the properties of an object created from the class.
*/

class Car{
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

let myCar = new Car();
console.log(myCar);

// Values of properties may be assigned after creation/instantiation of an object.
myCar.brand = "Isuzu";
myCar.name = "Crosswind";
myCar.year = 2010;

console.log(myCar);

// Creating/instantiating a new object from the car class with initialize values

let myNewCar = new Car("Toyota", "Vios", 2021);

console.log(myNewCar);

// 9. Ternary operator
// Conditional operator 
// It has three operands
// (condition) ? expressionTrue : expressionFalse;

/*
	if(condition){
		statement/s;
	}
	else{
		statement/s;
	}

	(condition) ? expressionIfTrue : expressionIfFalse;
*/